'use strict';

module.exports = ({sequelize, DataTypes, Model, NOW}) => {

  class Pricing extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.TourPackage);
      this.belongsTo(models.UserAdmin);
    }
  };

  Pricing.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    priceDate: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: NOW
    },
    price: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    discountPercent: {
      type: DataTypes.DECIMAL(4,4),
      defaultValue: 0
    },
    discountFix: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }

  }, {
    sequelize,
    modelName: 'Pricing',
    tableName: 'Pricing',
    timestamps: true
  });

  return Pricing;
};