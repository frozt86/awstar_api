'use strict';

// password crypto
const crypto = require('crypto');
const csecret = `rahasiaKan`;

module.exports = ({sequelize, DataTypes, Model}) => {

  class UserAdmin extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.TourPackage);
      this.hasMany(models.Booking);
      this.hasMany(models.Pricing);
      this.hasMany(models.Invoice);
      this.hasMany(models.Payment);
    }
  };

  UserAdmin.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    fullName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    shortName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phone: {
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING(511),
      allowNull: false
    },
    isSuperAdmin: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false
    }

  }, {
    sequelize,
    modelName: 'UserAdmin',
    tableName: 'UserAdmin',
    timestamps: true,
    hooks: {
      beforeValidate: (userAdmin, options) => {
          // encrypt
          var hmac = crypto.createHmac('sha256', csecret);
          hmac.update(userAdmin.password);
          userAdmin.password = hmac.digest(`base64`);
      }
    }
  });

  return UserAdmin;
};