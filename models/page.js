'use strict';

module.exports = ({sequelize, DataTypes, Model, NOW, nanoid}) => {

  class Page extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.UserAdmin);
      this.belongsToMany(models.Component, { through: 'PageComponent' });
      this.hasMany(models.PageCategory);
    }
  };

  Page.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    pageCode: {
      type: DataTypes.STRING(64),
      unique: true,
      allowNull: false,
    },
    pageName: {
      type: DataTypes.STRING(511),
      allowNull: false,
    },
    pageTitle: {
      type: DataTypes.STRING(511),
      allowNull: false,
    },
    pageMetaName: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    pagePath: {
      type: DataTypes.TEXT,
      unique: true,
      allowNull: false,
    },
    publishDate: {
      type: DataTypes.DATE,
      defaultValue: NOW,
      allowNull: false,
    },
    notes: {
      type: DataTypes.TEXT
    },
    publishStatus: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: 0
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false
    }

  }, {
    sequelize,
    modelName: 'Page',
    tableName: 'Page',
    timestamps: true,
    indexes: [{ unique: true, fields: ['pageCode'] }],
    hooks: {
      beforeValidate: (page, options) => {
        // generated default values
        page.pageCode = nanoid(12);
      }
    }
  });

  return Page;
};