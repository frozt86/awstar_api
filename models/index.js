'use strict';

const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];

/*** NPM LIBRARY ***/
const { Sequelize, DataTypes, Model, NOW } = require('sequelize');


/*** EXPORTS ***/
module.exports = ({moment, formatNumber, nanoid}) => {

  // SEQUELIZE INSTANCE
  const sequelize = new Sequelize(config.database, config.username, config.password, config);

  let db = {};

  fs.readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {

    // import models
    const model = require(path.join(__dirname, file))({
      sequelize,
      DataTypes,
      Model,
      NOW,
      nanoid,
      moment,
      formatNumber
    });
    db[model.name] = model;
  });

  Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
      db[modelName].associate(db);
    }
  });

  return sequelize;
}