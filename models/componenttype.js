'use strict';

module.exports = ({sequelize, DataTypes, Model}) => {

  class ComponentType extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Component);
    }
  };

  ComponentType.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    componentTypeName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false
    }

  }, {
    sequelize,
    modelName: 'ComponentType',
    tableName: 'ComponentType',
    timestamps: true,
  });

  return ComponentType;
};