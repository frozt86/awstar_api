'use strict';

module.exports = ({sequelize, DataTypes, Model, NOW, nanoid}) => {

  class Payment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Booking);
      this.belongsTo(models.Invoice);
      this.belongsTo(models.UserAdmin);
    }
  };

  Payment.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    paymentCode: {
      type: DataTypes.STRING(64),
      unique: true,
      allowNull: false,
    },
    paymentDate: {
      type: DataTypes.DATE,
      defaultValue: NOW,
      allowNull: false
    },
    paymentChannel: {
      type: DataTypes.STRING,
      allowNull: false
    },
    payerName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    payerPhone: {
      type: DataTypes.STRING
    },
    payerEmail: {
      type: DataTypes.STRING
    },
    totalPayment: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    paymentNote: {
      type: DataTypes.STRING,
    },
    paymentStatus: {
      type: DataTypes.INTEGER(2),
      defaultValue: 0,
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }

  }, {
    sequelize,
    modelName: 'Payment',
    tableName: 'Payment',
    indexes: [{ unique: true, fields: ['paymentCode'] }],
    timestamps: true,
    hooks: {
      beforeValidate: (payment, options) => {
        // generated default values
        invoice.paymentCode = nanoid(12);
      }
    }
  });

  return Payment;
};