'use strict';

module.exports = ({sequelize, DataTypes, Model, NOW, nanoid}) => {

  class BookingCancelation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Booking);
      this.belongsTo(models.UserAdmin);
    }
  };

  BookingCancelation.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    cancelationCode: {
      type: DataTypes.STRING(64),
      unique: true,
      allowNull: false,
    },
    cancelationDate: {
      type: DataTypes.DATE,
      defaultValue: NOW,
      allowNull: false
    },
    cancelerName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    cancelerPhone: {
      type: DataTypes.STRING
    },
    cancelerEmail: {
      type: DataTypes.STRING
    },
    cancelationNote: {
      type: DataTypes.STRING,
    },
    cancelationStatus: {
      type: DataTypes.INTEGER(2),
      defaultValue: 0,
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }

  }, {
    sequelize,
    modelName: 'BookingCancelation',
    tableName: 'BookingCancelation',
    timestamps: true,
    indexes: [{ unique: true, fields: ['cancelationCode'] }],
    hooks: {
      beforeValidate: (invoice, options) => {
        // generated default values
        invoice.cancelationCode = nanoid(12);
      }
    }
  });

  return BookingCancelation;
};