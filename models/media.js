'use strict';

module.exports = ({sequelize, DataTypes, Model, NOW, nanoid}) => {

  class Media extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.UserAdmin);
    }
  };

  Media.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    mediaCode: {
      type: DataTypes.STRING(64),
      unique: true,
      allowNull: false,
    },
    mediaPath: {
      type: DataTypes.TEXT,
      unique: true,
      allowNull: false,
    },
    mediaType: {
      type: DataTypes.STRING,
      allowNull: false
    },
    publishDate: {
      type: DataTypes.DATE,
      defaultValue: NOW,
      allowNull: false,
    },
    notes: {
      type: DataTypes.TEXT
    },
    publishStatus: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: 0
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false
    }

  }, {
    sequelize,
    modelName: 'Media',
    tableName: 'Media',
    timestamps: true,
    indexes: [{ unique: true, fields: ['mediaCode'] }],
    hooks: {
      beforeValidate: (media, options) => {
        // generated default values
        media.mediaCode = nanoid(12);
      }
    }
  });

  return Media;
};