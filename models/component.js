'use strict';

module.exports = ({sequelize, DataTypes, Model, NOW, nanoid}) => {

  class Component extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.UserAdmin);
      this.belongsToMany(models.Page, { through: 'PageComponent' });
      this.hasOne(models.ComponentType);
    }
  };

  Component.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    componentCode: {
      type: DataTypes.STRING(64),
      unique: true,
      allowNull: false,
    },
    publishDate: {
      type: DataTypes.DATE,
      defaultValue: NOW,
      allowNull: false,
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    notes: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    publishStatus: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      defaultValue: 0
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false
    }

  }, {
    sequelize,
    modelName: 'Component',
    tableName: 'Component',
    timestamps: true,
    indexes: [{ unique: true, fields: ['componentCode'] }],
    hooks: {
      beforeValidate: (component, options) => {
        // generated default values
        component.componentCode = nanoid(12);
      }
    }
  });

  return Component;
};