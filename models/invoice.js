'use strict';

module.exports = ({sequelize, DataTypes, Model, NOW, nanoid}) => {

  class Invoice extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Booking);
      this.belongsTo(models.UserAdmin);
    }
  };

  Invoice.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    invoiceCode: {
      type: DataTypes.STRING(64),
      unique: true,
      allowNull: false,
    },
    invoiceDate: {
      type: DataTypes.DATE,
      defaultValue: NOW,
      allowNull: false
    },
    invoiceEndDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    paymentChannel: {
      type: DataTypes.STRING,
      allowNull: false
    },
    midtransCode: {
      type: DataTypes.STRING,
    },
    midtransUrl: {
      type: DataTypes.TEXT,
    },
    invoiceeName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    invoiceePhone: {
      type: DataTypes.STRING
    },
    invoiceeEmail: {
      type: DataTypes.STRING
    },
    totalInvoice: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    invoiceNote: {
      type: DataTypes.STRING,
    },
    invoiceStatus: {
      type: DataTypes.INTEGER(2),
      defaultValue: 0,
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }

  }, {
    sequelize,
    modelName: 'Invoice',
    tableName: 'Invoice',
    indexes: [{ unique: true, fields: ['invoiceCode'] }],
    timestamps: true,
    hooks: {
      beforeValidate: (invoice, options) => {
        // generated default values
        invoice.invoiceCode = nanoid(12);
      }
    }
  });

  return Invoice;
};