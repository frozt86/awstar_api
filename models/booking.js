'use strict';

module.exports = ({sequelize, DataTypes, Model, NOW, nanoid}) => {

  class Booking extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.UserAdmin);
      this.belongsTo(models.TourPackage);
      this.hasOne(models.Payment);
      this.hasOne(models.Invoice);
    }
  };

  Booking.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    bookingCode: {
      type: DataTypes.STRING(64),
      unique: true,
      allowNull: false,
    },
    bookingDate: {
      type: DataTypes.DATE,
      defaultValue: NOW,
      allowNull: false,
    },
    custName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    custPhone: {
      type: DataTypes.STRING
    },
    custEmail: {
      type: DataTypes.STRING
    },
    totalInvoice: {
      type: DataTypes.BIGINT
    },
    bookingStatus: {
      type: DataTypes.INTEGER(2),
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
      allowNull: false
    }

  }, {
    sequelize,
    modelName: 'Booking',
    tableName: 'Booking',
    timestamps: true,
    indexes: [{ unique: true, fields: ['bookingCode'] }],
    hooks: {
      beforeValidate: (booking, options) => {
        // generated default values
        booking.bookingCode = nanoid(12);
      }
    }
  });

  return Booking;
};