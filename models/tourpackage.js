'use strict';

module.exports = ({sequelize, DataTypes, Model}) => {

  class TourPackage extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.UserAdmin);
      this.hasMany(models.Booking);
      this.hasMany(models.Pricing);
    }
  };

  TourPackage.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    packageName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    brochureFile: {
      type: DataTypes.STRING(511),
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }

  }, {
    sequelize,
    modelName: 'TourPackage',
    tableName: 'TourPackage',
    timestamps: true
  });

  return TourPackage;
};