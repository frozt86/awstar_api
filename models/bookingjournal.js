'use strict';

module.exports = ({sequelize, DataTypes, Model, NOW}) => {

  class BookingJournal extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Booking);
      this.belongsTo(models.UserAdmin);
    }
  };

  BookingJournal.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    journalDate: {
      type: DataTypes.DATE,
      defaultValue: NOW,
      allowNull: false
    },
    journalInformation: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }

  }, {
    sequelize,
    modelName: 'BookingJournal',
    tableName: 'BookingJournal',
    timestamps: true,
  });

  return BookingJournal;
};