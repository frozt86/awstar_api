'use strict';

module.exports = ({sequelize, DataTypes, Model, nanoid}) => {

  class BookingParticipant extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Booking);
    }
  };

  BookingParticipant.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    participantCode: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING
    },
    gender: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    age: {
      type: DataTypes.INTEGER(3),
      allowNull: false
    },
    natioanlity: {
      type: DataTypes.STRING,
      allowNull: false
    },
    identityType: {
      type: DataTypes.STRING
    },
    identityNumber: {
      type: DataTypes.STRING
    },
    homeTown: {
      type: DataTypes.STRING
    },
    active: {
      type: DataTypes.BOOLEAN
    }

  }, {
    sequelize,
    timestamps: true,
    modelName: 'BookingParticipant',
    tableName: 'BookingParticipant',
    indexes: [{ unique: true, fields: ['participantCode'] }],
    hooks: {
      beforeValidate: (participant, options) => {
        // generated default values
        participant.participantCode = nanoid(12);
      }
    }
  });

  return BookingParticipant;
};