'use strict';

/*** .env ***/
require('dotenv').config();

/*** NPM LIBRARY ***/


/*** MAIN CODE ***/
(async ()=>{

  /*** LOCAL LIBRARY ***/
  const apiApp = require('./graphqlapiapp.js');

  apiApp();

})().catch(err=>console.log(`server.js - ERROR -> `, err));