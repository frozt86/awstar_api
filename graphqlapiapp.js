'use strict';

/*** START API APP ***/
module.exports = async function()  {

  /*** NPM LIBRARY ***/
  const express = require('express');
  const { nanoid } = require('nanoid');
  const moment = require('moment');
  const format = require('format-number');
  const winston = require('winston');



  /*** VARIABLES ***/
  // express App
  const expressApp = express();

  // format Number
  const formatNumber = format({decimal : ",", integerSeparator : "."});

  // logger
  const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
      //
      // - Write all logs with level `error` and below to `error.log`
      // - Write all logs with level `info` and below to `combined.log`
      //
      new winston.transports.File({ filename: 'error.log', level: 'error' }),
      new winston.transports.File({ filename: 'combined.log' })
    ],
  });

  // ORM Sequelize Instance
 const sequelize = require('./models')({moment, formatNumber, nanoid});

  /*** EXPRESS APP ***/
  //graphql
  require('./midwares/graphqlmidwares.js')({expressApp, sequelize});

  // Static Public
  expressApp.use('/resources', express.static('./resources'));


  let port = parseInt(process.env.PORT);
  expressApp.listen(port, () => console.log(`API ${process.pid} Started.\nlistening on ${port}`))

};