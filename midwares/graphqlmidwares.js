var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');

module.exports = function ({expressApp, sequelize}) {

  // Construct a schema, using GraphQL schema language
  var schema = buildSchema(`
    type Query {
      hello: String
    }
  `);

  // The root provides a resolver function for each API endpoint
  var root = {
    hello: () => {
      return 'Hello world!';
    },
  };

  expressApp.use(`/`, graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: (process.env.NODE_ENV === 'development'),
  }));

}